const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const TrackSchema = new Schema({
  title: {
    type: String,
    require: true
  },
  album: {
    type: Schema.Types.ObjectId,
    ref: 'Album',
    required: true,
  },
  time: String,
});

const Track = mongoose.model('Track', TrackSchema);

module.exports = Track;