const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const AlbumSchema = new Schema({
  title: {
    type: String,
    require: true
  },
  artist: {
    type: Schema.Types.ObjectId,
    ref: 'Artist',
    required: true,
  },
  date: String,
  image: String
});

const Album = mongoose.model('Album', AlbumSchema);

module.exports = Album;