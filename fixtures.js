const mongoose = require('mongoose');
const config = require('./config');

const Artist = require('./models/Artist');
const Album = require('./models/Album');
const Track = require('./models/Track');

const run = async () => {
  await mongoose.connect(config.dbUrl, config.mongoOptions);

  const connection = mongoose.connection;

  const collections = await connection.db.collections();

  for (let collection of collections) {
    await collection.drop();
  }

  const artists = await Artist.create(
    {
      name: 'Михаи́л Ю́рьевич Горшенёв',
      photo: 'gorshenev.png',
      description: 'Советский и российский музыкант, лидер рок-группы «Король и Шут». ' +
        'Старший брат лидера рок-группы «Кукрыниксы» Алексея Горшенёва'
    },
    {
      name: 'Ангус МакКиннон Янг',
      photo: 'yang.jpeg',
      description: 'Бессменный соло-гитарист и автор песен австралийской рок-группы AC/DC. ' +
        'Янг известен своим мастерством игры на гитаре, дикой энергией на сцене и школьной униформой.'
    },
    {
      name: 'Тилль Ли́ндеманн',
      photo: 'til.jpg',
      description: 'Немецкий вокалист, автор текстов песен и фронтмен метал-групп Rammstein и Lindemann, поэт.' +
        ' Принимал участие в записи некоторых песен групп Apocalyptica и Puhdys. ' +
        'Автор сборников стихов «Messer», «In stillen Nächten», снялся в 8 фильмах. Имеет образование пиротехника.'
    },
  );

  const albums = await Album.create(
    {
      title: 'Как в старой сказке',
      artist: artists[0]._id,
      date: '2001',
      image: 'skazka.jpg'
    },
    {
      title: 'Highway to Hell',
      artist: artists[1]._id,
      date: '1979',
      image: 'acdc.png'
    },
    {
      title: 'Mutter',
      artist: artists[2]._id,
      date: '2001',
      image: 'mutter.jpeg'
    },
  );

  await Track.create(
    {
      title: 'Проклятый старый дом',
      album: albums[0]._id,
      time: '4:17',
    },
    {
      title: 'Тайна хозяйки старинных часов',
      album: albums[0]._id,
      time: '3:31',
    },
    {
      title: 'Возвращение колдуна',
      album: albums[0]._id,
      time: '3:15',
    },
    {
      title: 'Highway to Hell',
      album: albums[1]._id,
      time: '3:28',
    },
    {
      title: ' If You Want Blood ',
      album: albums[1]._id,
      time: '4:37',
    },
    {
      title: 'Touch Too Much',
      album: albums[1]._id,
      time: '4:26',
    },
    {
      title: 'Mutter',
      album: albums[2]._id,
      time: '4:32',
    },
    {
      title: 'Ich Will',
      album: albums[2]._id,
      time: '3:38',
    },
    {
      title: 'Feuer Frei',
      album: albums[2]._id,
      time: '3:11',
    },
  );

  await connection.close();
};

run().catch(error => {
  console.log('Something went wrong', error);
});